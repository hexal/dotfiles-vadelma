# Ansible dotfiles configuration (Raspberry Pi OS Lite)

# Installation

- Update package list and upgrade the system
    - `sudo apt update`
    - `sudo apt upgrade`
- Set up wifi, ssh (Interface Options > SSH) and other settings, if necessary
    - `sudo raspi-config`
- Install git
    - `sudo apt install git`
- Install ansible
    - `sudo apt install ansible`
- Install ansible playbook(s)
    - `ansible-playbook -i hosts -K playbook-pi.yml`
- Add a samba user
    - sudo smbpasswd -a username